ARG ARCH=amd64
FROM $ARCH/python:alpine

MAINTAINER Michael Balser <michael@balser.cc>

# args
ARG VCS_REF
ARG BUILD_DATE

# labels
LABEL maintainer="Michael Balser <michael@balser.cc>" \
  org.label-schema.schema-version="1.0" \
  org.label-schema.name="michaelbalser/imagededup" \
  org.label-schema.description="imagededup docker image" \
  org.label-schema.version="0.1" \
  org.label-schema.url="https://hub.docker.com/repository/docker/michaelbalser/imagededup" \
  org.label-schema.vcs-url="https://gitlab.com/mbalser1/imagededup" \
  org.label-schema.vcs-ref=$VCS_REF \
  org.label-schema.build-date=$BUILD_DATE

# install packages
RUN set -ex \
    && apk add --no-cache --update --virtual .build-deps \
        gcc python3-dev linux-headers g++ make build-base \
        gfortran openblas-dev lapack-dev cython \
    && apk add --no-cache --virtual .run-deps \
        tini \
    && pip3 install imagededup \
    && apk del .build-deps

ENTRYPOINT ["/sbin/tini", "--"]
